import express from 'express';

const app = express();


app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/task2A', (req, res) => {
  const a = +req.query.a || 0;
  const b = +req.query.b || 0;

  res.send('' + (a + b));
});

app.post('/task2A', (req, res) => {
  const a = +req.query.a || 0;
  const b = +req.query.b || 0;

  res.send('' + (a + b));
});


app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
